class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = nil
  end

  def setup
    word_length = referee.pick_secret_word
    guesser.register_secret_length(word_length)
    @board = Array.new(word_length)
  end

  def take_turn
    letter = guesser.guess(board)
    index = referee.check_guess(letter)
    update_board(index, letter)
    guesser.handle_response(letter, index)
  end

  def update_board(index, ltr)
    index.each { |i| @board[i] = ltr }
  end

end

class HumanPlayer

  def pick_secret_word
    puts "Choose a word..."
    gets.chomp.length
  end

  def check_guess(letter)
    puts "What indices contain the letter #{letter}?"
    input = gets.chomp
    input.split(",").map(&:to_i)
  end

  def register_secret_length(word_length)
    puts "The secret word has #{word_length} letters."
  end

  def guess(board)
    print "Guess a letter from a to z: "
    gets.chomp
  end

  def handle_response(guess, indices)
    if indices.empty?
      puts "#{guess} is not a letter in the secret word."
    else
      puts "Found #{guess} at indices #{indices}."
    end
  end
end

class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary
    @secret_word = nil
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(char)
    arr = []
    @secret_word.chars.each_with_index { |c, i| arr << i if c == char }
    arr
  end

  def register_secret_length(len)
    @secret_length = len
    @dictionary.select! do |w|
      w.length == @secret_length
    end
  end

  def guess(board)
    letter_freq = Hash.new(0)
    @dictionary.each do |w|
      w.chars.each do |l|
        letter_freq[l] += 1
      end
    end
    ('abcdefghijklmnopqrstuvwxyz'.chars - board).reduce do |acc, chr|
      letter_freq[chr] > letter_freq[acc] ? chr : acc
    end
  end

  def handle_response(letter, inds)
    if inds.empty?
      @dictionary.reject! do |w|
        w =~ /#{letter}/
      end
    else
      partial = Array.new(@secret_length)
      inds.each { |i| partial[i] = letter }
      reg = partial.map { |c| c.nil? ? "[^#{letter}]" : c }.join
      @dictionary.select! do |w|
        w =~ /^#{reg}$/
      end
    end
  end

  def candidate_words
    @dictionary
  end
end
